package br.com.coffeetek.Ui;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import br.com.coffeetek.Model.Coffee;
import br.com.coffeetek.Model.ShoppingCartItem;
import br.com.coffeetek.Model.ShoppingCartList;
import br.com.coffeetek.R;
import br.com.coffeetek.Util.mImageDecoder;

public class DetailActivity extends AppCompatActivity {

    private TextView product_name, product_content;
    private ImageView cup_small, cup_medium, cup_large, sugar_no,
            sugar_small, sugar_medium, sugar_large, aditional_cream, aditional_cinnamon,
            aditional_milk, aditional_coffee, aditional_chocolate, image_product;
    private ImageButton btn_add, btn_remove;
    private EditText txt_quantity;
    private Button btn_add_to_cart;

    private List<String> mCofee = new ArrayList<>();
    List<ShoppingCartItem> mS = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        mS.clear();

        //Configurações da toolbar e botão up
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.arrow_left);
        upArrow.setColorFilter(getResources().getColor(R.color.colorTitle), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //Objeto que é passado no intent do menu
        final Coffee intentObject = (Coffee) getIntent().getSerializableExtra("coffee");

        //Carrega a imagem do banner
        image_product = findViewById(R.id.image_product);
        image_product.setImageBitmap(mImageDecoder.decodeBase64(intentObject.getImgUrl()));

        // Optei por definir uma lista vazia sempre que o produto por padrão não possua adicionais
        // para não precisar me precupar com erros ao buscar por um obejto nulo.
        if (intentObject.getAdditional() == null) {
            intentObject.setAdditional(mCofee);
        } else {
            mCofee = intentObject.getAdditional();
        }

        product_name = findViewById(R.id.product_name_title);
        product_name.setText(intentObject.getTitle());

        product_content = findViewById(R.id.product_content);

        txt_quantity = findViewById(R.id.txt_quantity);
        btn_add_to_cart = findViewById(R.id.btn_add_to_cart);

        //Eventos de clicks para os botões + e -
        btn_add = findViewById(R.id.btn_add);
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                BigDecimal q = new BigDecimal(txt_quantity.getText().toString());
                q = q.add(new BigDecimal("1"));
                txt_quantity.setText(String.valueOf(q));
            }
        });

        btn_remove = findViewById(R.id.btn_remove);
        btn_remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (new Integer(txt_quantity.getText().toString()) > 1) {
                    BigDecimal q = new BigDecimal(txt_quantity.getText().toString());
                    q = q.subtract(new BigDecimal("1"));
                    txt_quantity.setText(String.valueOf(q));
                }
            }
        });

        //Eventos de clicks para os adicionais
        aditional_cream = findViewById(R.id.aditional_cream);
        aditional_cream.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCofee.contains("chantilly")) {
                    mCofee.remove("chantilly");
                    aditional_cream.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                } else {
                    mCofee.add("chantilly");
                    aditional_cream.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));

                }

            }

        });

        aditional_cinnamon = findViewById(R.id.aditional_cinnamon);
        aditional_cinnamon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCofee.contains("cinnamon")) {
                    mCofee.remove("cinnamon");
                    aditional_cinnamon.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                } else {
                    mCofee.add("cinnamon");
                    aditional_cinnamon.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));

                }

            }
        });

        aditional_chocolate = findViewById(R.id.aditional_chocolate);
        aditional_chocolate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCofee.contains("chocolate")) {
                    mCofee.remove("chocolate");
                    aditional_chocolate.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                } else {
                    mCofee.add("chocolate");
                    aditional_chocolate.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));

                }

            }
        });

        aditional_coffee = findViewById(R.id.aditional_coffee);
        aditional_coffee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCofee.contains("coffee")) {
                    mCofee.remove("coffee");
                    aditional_coffee.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                } else {
                    mCofee.add("coffee");
                    aditional_coffee.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));

                }

            }
        });

        aditional_milk = findViewById(R.id.aditional_milk);
        aditional_milk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (mCofee.contains("milk")) {
                    mCofee.remove("milk");
                    aditional_milk.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                } else {
                    mCofee.add("milk");
                    aditional_milk.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));

                }

            }
        });

        cup_small = findViewById(R.id.cup_small);
        cup_medium = findViewById(R.id.cup_medium);
        cup_large = findViewById(R.id.cup_large);

        sugar_no = findViewById(R.id.sugar_no);
        sugar_small = findViewById(R.id.sugar_small);
        sugar_medium = findViewById(R.id.sugar_medium);
        sugar_large = findViewById(R.id.sugar_large);

        txt_quantity = findViewById(R.id.txt_quantity);

        cup_small.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentObject.getSize() != 0) {
                    intentObject.setSize(0);
                    cup_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                    cup_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    cup_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    product_content.setText(getResources().getString(R.string.txt_content_small));
                }
            }
        });

        cup_medium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentObject.getSize() != 1) {
                    intentObject.setSize(1);
                    cup_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    cup_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                    cup_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    product_content.setText(getResources().getString(R.string.txt_content_medium));
                }
            }
        });

        cup_large.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentObject.getSize() != 2) {
                    intentObject.setSize(2);
                    cup_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    cup_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    cup_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                    product_content.setText(getResources().getString(R.string.txt_content_large));
                }
            }
        });

        sugar_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentObject.getSugar() != 0) {
                    intentObject.setSugar(0);
                    sugar_no.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                    sugar_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                }
            }
        });

        sugar_small.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentObject.getSugar() != 1) {
                    intentObject.setSugar(1);
                    sugar_no.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                    sugar_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                }
            }
        });

        sugar_medium.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentObject.getSugar() != 2) {
                    intentObject.setSugar(2);
                    sugar_no.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                    sugar_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                }
            }
        });

        sugar_large.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (intentObject.getSugar() != 3) {
                    intentObject.setSugar(3);
                    sugar_no.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorDesabled));
                    sugar_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                }
            }
        });

        btn_add_to_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                ShoppingCartItem mItem = new ShoppingCartItem(intentObject.getTitle(),
                        intentObject.getImgUrl(), intentObject.getSize(), intentObject.getSugar(),
                        Long.valueOf(txt_quantity.getText().toString()), mCofee);

                mS = ShoppingCartList.getInstance().getItemsList();
                mS.add(mItem);

                ShoppingCartList.getInstance().setItemsList(mS);

            }
        });

        //Carrega a configuração padrão do produto
        setProductDefault(intentObject, mCofee);

    }

    public void setProductDefault(Coffee coffee, List<String> mAdd) {

        try {

            if (coffee.getSize() == 0) {
                cup_small = findViewById(R.id.cup_small);
                cup_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
            } else if (coffee.getSize() == 1) {
                cup_medium = findViewById(R.id.cup_medium);
                cup_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
            } else if (coffee.getSize() == 2) {
                cup_large = findViewById(R.id.cup_large);
                cup_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
            }

            if (coffee.getSugar() == 0) {
                sugar_no = findViewById(R.id.sugar_no);
                sugar_no.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
            } else if (coffee.getSugar() == 1) {
                sugar_small = findViewById(R.id.sugar_small);
                sugar_small.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
            } else if (coffee.getSugar() == 2) {
                sugar_medium = findViewById(R.id.sugar_medium);
                sugar_medium.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
            } else if (coffee.getSugar() == 3) {
                sugar_large = findViewById(R.id.sugar_large);
                sugar_large.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
            }

            if (coffee.getAdditional().size() > 0) {

                if (mAdd.contains("chantilly")) {
                    aditional_cream.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                }
                if (mAdd.contains("cinnamon")) {
                    aditional_cinnamon.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                }
                if (mAdd.contains("chocolate")) {
                    aditional_chocolate.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                }
                if (mAdd.contains("coffee")) {
                    aditional_coffee.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                }
                if (mAdd.contains("milk")) {
                    aditional_milk.setColorFilter(ContextCompat.getColor(getApplicationContext(), R.color.colorTitle));
                }

            }

        } catch (Exception e) {

            e.printStackTrace();

        }
    }

}
