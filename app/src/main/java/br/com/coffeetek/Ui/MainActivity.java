package br.com.coffeetek.Ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.coffeetek.Adapters.ProductListAdapter;
import br.com.coffeetek.Decorator.SimpleDivider;
import br.com.coffeetek.Model.Coffee;
import br.com.coffeetek.R;

public class MainActivity extends AppCompatActivity {

    Context context;
    private final String URL = "https://desafio-mobility.herokuapp.com/products.json";

    List<Coffee> mProductList;

    ImageView image_shoppingcart;

    private RecyclerView mRecyclerProduct;
    private ProductListAdapter mProductListAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mProductList = new ArrayList<>();

        mProductListAdapter = new ProductListAdapter(mProductList, context);

        mRecyclerProduct = findViewById(R.id.mRecycler);
        mRecyclerProduct.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mRecyclerProduct.setItemAnimator(new DefaultItemAnimator());
        mRecyclerProduct.addItemDecoration(new SimpleDivider(getApplicationContext()));
        mRecyclerProduct.setAdapter(mProductListAdapter);

        image_shoppingcart = findViewById(R.id.image_shoppingcart);
        image_shoppingcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), CartActivity.class);
                startActivity(intent);

            }
        });

        getData();

    }

    private void getData() {

        StringRequest productsJSON = new StringRequest(Request.Method.GET, URL, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {

                try {

                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray lista = jsonObject.getJSONArray("products");

                    for (int i = 0; i < lista.length(); i++) {

                        Coffee coffee = new Coffee();
                        JSONObject item = lista.getJSONObject(i);

                        coffee.setSugar(item.getLong("sugar"));
                        coffee.setSize(item.getLong("size"));
                        coffee.setTitle(item.getString("title"));
                        coffee.setImgUrl(item.getString("image"));

                        try {

                            JSONArray additional = lista.getJSONObject(i).getJSONArray("additional");

                            if (additional.length() > 0) {

                                List<String> additionalList = new ArrayList<>();

                                for (int size = 0; size < additional.length(); size++) {

                                    additionalList.add(additional.getString(size));

                                }

                                coffee.setAdditional(additionalList);

                            }

                        } catch (JSONException e) {

                            e.printStackTrace();

                        }

                        mProductList.add(coffee);

                    }

                } catch (JSONException e) {

                    e.printStackTrace();

                }

                mProductListAdapter.notifyDataSetChanged();

            }

        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                error.getMessage();

            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(productsJSON);

    }

}