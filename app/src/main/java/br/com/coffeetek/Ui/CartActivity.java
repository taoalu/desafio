package br.com.coffeetek.Ui;

import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

import br.com.coffeetek.Adapters.ProductListAdapter;
import br.com.coffeetek.Adapters.ShoppingCartAdapter;
import br.com.coffeetek.Decorator.SimpleDivider;
import br.com.coffeetek.Model.ShoppingCartItem;
import br.com.coffeetek.Model.ShoppingCartList;
import br.com.coffeetek.R;

public class CartActivity extends AppCompatActivity {

    Context context;

    List<ShoppingCartItem> mProductList;

    private TextView txt_empty;
    private ImageView nav_uparrow;
    private RecyclerView mShoppingCartRecycler;
    private ShoppingCartAdapter mShoppingCartAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shoppingcart);

        if (ShoppingCartList.getInstance().getItemsList().size() == 0) {
            findViewById(R.id.txt_empty).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.txt_empty).setVisibility(View.INVISIBLE);
        }

        mProductList = ShoppingCartList.getInstance().getItemsList();

        Toolbar toolbar = findViewById(R.id.mToolBar);
        setSupportActionBar(toolbar);

        final Drawable upArrow = getResources().getDrawable(R.drawable.arrow_left);
        upArrow.setColorFilter(getResources().getColor(R.color.colorTitle), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mShoppingCartAdapter = new ShoppingCartAdapter(mProductList, context);

        mShoppingCartRecycler = findViewById(R.id.mRecycler);
        mShoppingCartRecycler.setLayoutManager(new LinearLayoutManager(getApplicationContext()));
        mShoppingCartRecycler.setItemAnimator(new DefaultItemAnimator());
        mShoppingCartRecycler.setAdapter(mShoppingCartAdapter);

    }
}
