package br.com.coffeetek.Adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.coffeetek.Model.ShoppingCartItem;
import br.com.coffeetek.R;
import br.com.coffeetek.Util.mImageDecoder;

public class ShoppingCartAdapter extends RecyclerView.Adapter<ShoppingCartAdapter.ViewHolder> {

    private Context context;
    private List<ShoppingCartItem> mProductList;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView product_name, product_quanity, product_additional;
        private ImageView product_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            product_name = itemView.findViewById(R.id.product_name);
            product_quanity = itemView.findViewById(R.id.product_quanity);
            product_additional = itemView.findViewById(R.id.product_additional);
            product_image = itemView.findViewById(R.id.product_image);

        }
    }

    public ShoppingCartAdapter(List<ShoppingCartItem> mProductList, Context context) {
        this.mProductList = mProductList;
        this.context = context;
    }

    @NonNull
    @Override
    public ShoppingCartAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.shopping_cart_item_card_layout, viewGroup, false);
        ShoppingCartAdapter.ViewHolder holder = new ShoppingCartAdapter.ViewHolder(itemView);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ShoppingCartAdapter.ViewHolder holder, final int position) {

        final ShoppingCartItem cartItem = mProductList.get(position);

        holder.product_name.setText(cartItem.getTitle());
        holder.product_quanity.setText(String.valueOf(cartItem.getQuantity()));
        holder.product_additional.setText(String.valueOf(cartItem.getAdditional()));
        holder.product_image.setImageBitmap(mImageDecoder.decodeBase64(cartItem.getImgUrl()));

    }

    @Override
    public int getItemCount() {

        return mProductList.size();

    }

    public String removeChar(String original) {

        original.replace("[", "");
        original.replace("]", "");

        return original;

    }
}
