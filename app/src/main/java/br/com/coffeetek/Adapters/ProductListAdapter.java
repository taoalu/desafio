package br.com.coffeetek.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import br.com.coffeetek.Model.Coffee;
import br.com.coffeetek.R;
import br.com.coffeetek.Ui.DetailActivity;
import br.com.coffeetek.Util.mImageDecoder;

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.ViewHolder> {

    private Context context;
    private List<Coffee> mProductList;

    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView product_name;
        private ImageView product_image;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            product_name = itemView.findViewById(R.id.product_name_title);
            product_image = itemView.findViewById(R.id.product_image);

        }
    }

    public ProductListAdapter(List<Coffee> mProductList, Context context) {
        this.mProductList = mProductList;
        this.context = context;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.product_list_item_card_layout, viewGroup, false);
        ViewHolder holder = new ViewHolder(itemView);

        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        final Coffee coffee = mProductList.get(position);

        holder.product_name.setText(coffee.getTitle());
        holder.product_image.setImageBitmap(mImageDecoder.decodeBase64(coffee.getImgUrl()));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Coffee mCoffee = mProductList.get(position);

                Intent intent = new Intent(v.getContext(), DetailActivity.class);
                intent.putExtra("coffee", mCoffee);
                v.getContext().startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {

        return mProductList.size();

    }

}
