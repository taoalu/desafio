package br.com.coffeetek.Model;

import java.io.Serializable;
import java.util.List;

public class Coffee implements Serializable {

    String title, imgUrl;
    long size, sugar;
    List<String> additional;

    public Coffee() {
    }

    public Coffee(String title, String imgUrl, long size, long sugar, List<String> additional) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.size = size;
        this.sugar = sugar;
        this.additional = additional;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getSugar() {
        return sugar;
    }

    public void setSugar(long sugar) {
        this.sugar = sugar;
    }

    public List<String> getAdditional() {
        return additional;
    }

    public void setAdditional(List<String> additional) {
        this.additional = additional;
    }
}
