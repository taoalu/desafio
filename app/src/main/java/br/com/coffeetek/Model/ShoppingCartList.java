package br.com.coffeetek.Model;

import java.util.ArrayList;
import java.util.List;

public class ShoppingCartList {

    private static final ShoppingCartList ourInstance = new ShoppingCartList();

    List<ShoppingCartItem> itemsList = new ArrayList<>();

    public static ShoppingCartList getInstance() {

        return ourInstance;

    }

    private ShoppingCartList() {

    }

    public ShoppingCartList(List<ShoppingCartItem> itemsList) {
        this.itemsList = itemsList;
    }

    public static ShoppingCartList getOurInstance() {
        return ourInstance;
    }

    public List<ShoppingCartItem> getItemsList() {
        return itemsList;
    }

    public void setItemsList(List<ShoppingCartItem> itemsList) {
        this.itemsList = itemsList;
    }
}