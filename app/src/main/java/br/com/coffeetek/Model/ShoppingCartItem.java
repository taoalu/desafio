package br.com.coffeetek.Model;

import java.util.List;

public class ShoppingCartItem {

    String title, imgUrl;
    long size, sugar, quantity;
    List<String> additional;

    public ShoppingCartItem() {
    }

    public ShoppingCartItem(String title, String imgUrl, long size, long sugar, long quantity, List<String> additional) {
        this.title = title;
        this.imgUrl = imgUrl;
        this.size = size;
        this.sugar = sugar;
        this.quantity = quantity;
        this.additional = additional;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public long getSize() {
        return size;
    }

    public void setSize(long size) {
        this.size = size;
    }

    public long getSugar() {
        return sugar;
    }

    public void setSugar(long sugar) {
        this.sugar = sugar;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public List<String> getAdditional() {
        return additional;
    }

    public void setAdditional(List<String> additional) {
        this.additional = additional;
    }
}
